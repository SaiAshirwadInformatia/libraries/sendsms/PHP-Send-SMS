const { description } = require('../../package')

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Send SMS Documentation',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: '',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: true,
    nav: [
      {
        text: 'API Documentation',
        link: '/api/',
      },
      {
        text: 'DLT Guide',
        link: '/dlt-guide/',
      },
      {
        text: 'GitLab',
        link: 'https://gitlab.com/SaiAshirwadInformatia/libraries/sendsms/PHP-Send-SMS',
      },
      {
        text: 'Company',
        link: 'https://saiashirwad.com'
      }
    ],
    sidebar: {
      '/api/': [
        {
          title: 'API Documentation',
          collapsable: false,
          children: [
            '',
            'Usage',
            'Encrypted',
            'DLTDevMode',
          ]
        }
      ],
      '/dlt-guide/': [
        {
          title: 'DLT Guide',
          collapsable: false,
          children: [
            '',
            'PingConnect'
          ]
        }
      ],
    }
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
  ]
}
