# Introduction

## Package Features

1. Validation of Country Code
2. Validation of mobile number by Country Code & known mobile prefix
3. Validation of mobile length by Country Code
4. Send Transaction SMS
5. Send Promotional SMS
6. Send OTP over SMS [Under Development]
7. Check balance easily

The verification feature is built on-top of authentic source [Wikipedia](https://en.wikipedia.org/wiki/List_of_mobile_telephone_prefixes_by_country)

## Benefits

1. Identify unknown numbers before, the library provides way's before you can even trigger SMS or wanted to register a number
2. Save on credits for sending on False number's, use them wisely with authentic numbers only
3. Easily integrate into any PHP application no matter what the framework is being used.

## Installation

```
composer require saiashirwadinformatia/sendsms
```

1. Register - [https://saiashirwad.in](https://saiashirwad.in)
1. Retrieve API Key - [https://saiashirwad.in/user/#api](https://saiashirwad.in/user/#api)


