# Development Mode DLT

DLT Approval for Template's may take time based on your message and other criteria.

If you have not initiated for DLT approval, please go through this [DLT Guide](/DLT-Guide)

```php
use SaiAshirwadInformatia\SendSMS;

$smsClient = new SendSMS;

// This function enables the Development Mode for DLT, so untill the message template is not approved you can send the SMS for testing
$smsClient->enableDevMode();

// You can also chain this functionality as
$smsClient->enableDevMode()
            ->send(/* ... Standard Params */);

// Default is disabled mode
// We recommend you to keep this disable to avoid closure of your account
// In-case, you want to disable during runtime, you can simple call this message on the same client
$smsClient->disableDevMode();
```