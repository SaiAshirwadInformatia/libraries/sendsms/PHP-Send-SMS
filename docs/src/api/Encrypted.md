# Encrypted

Often we send crucial information like OTP, Password in SMS. These are easily exposed in reports or logs. Even we as vendor have a way to store this values which could lead to privacy leak.

As a additional security measure, we now support encrypted messages due to which the SMS becomes secured even in Admin Panel or logs seen anywhere. This way we can assure you with privacy 100% taken care of.

```php
use SaiAshirwadInformatia\SendSMS;

$smsClient = new SendSMS;

// This function enables the encrypted security over SMS clients
$smsClient->enableEncryptedMessage();

// You can also chain this functionality as
$smsClient->enableEncryptedMessage()
            ->send(/* ... Standard Params */);

// In-case, you want to disable & verify messages in logs
// Messages sent post disable will only be available to read directly
// Messages sent prior encrypted will still not be available for reading
$smsClient->disableEncryptedMessage();
```