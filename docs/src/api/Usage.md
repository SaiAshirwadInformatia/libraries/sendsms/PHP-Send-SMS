# Usage

For security of credentials & identity, Send SMS package reads the `.env` or rather environment variables due to which the configs will stay secured outside git commits.

## Send SMS

```php
use SaiAshirwadInformatia\SendSMS;

/**
 * Retrieve your key from https://saiashirwad.in/user/#api
 *
 * Connect on support@saiashirwad.com for queries
 * 
 */
$apiKey = 'API_KEY';
// If API Key is given null, it will attempt to read from env as `SMS_SEND_KEY`

/**
 * Default is "91"
 */
$countryCode = '91';

/**
 * Seen on receivers phone a 6 characters Sender Id
 *
 * Default is "SAIMSG"
 *
 * Exact length required "6"
 */
$senderId = 'ABCDEF';

/**
 * Please note, this cannot be less than or greater than 6, should be exact 6 characters
 * Available Routes
 *  SendSMS::TRANSACTION_ROUTE => transaction (Received by DND)
 *  SendSMS::PROMOTIONAL_ROUTE => promotional (Ignored by DND)
 *
 * Please note, transaction require's explicit opt-in in your application
 * We may ask you this data, if any complains/reports received
 */
$route = SendSMS::TRANSACTION_ROUTE;

$smsClient = new SendSMS($apiKey, $countryCode, $senderId, $route);

/**
 * Mobile number
 *
 * This can also be comma separate string for multiple phone numbers
 */
$mobile = '8888888888';

/**
 * Message upto 160 characters is considered as "1" credit
 * If it goes beyond the desired limit's credit's are charged accordingly
 */
$message = 'Hello, this is your message!';
$countryCode = '91'; // dynamically change country code during each call
$senderId = 'TESTMM'; // dynamically set sender id for each SMS
$dltTemplateId = ''; // This is now mandatory to send SMS in authentic way
$success = $smsClient->send($dltTemplateId, $mobile, $message, $countryCode, $senderId);

// returns SMS Message Id for tracking
echo $success->getMessage();
```

## Check Balance

```php
use SaiAshirwadInformatia\SendSMS;

/**
 * Retrieve your key from https://saiashirwad.in/user/#api
 *
 * Connect on support@saiashirwad.com for queries
 */
$apiKey = 'API_KEY';

// Default is Transaction route, alternate is SendSMS::PROMOTIONAL_ROUTE
$route = SendSMS::TRANSACTION_ROUTE;

$smsClient = new SendSMS($apiKey);

// Fetch balance for selected route either transaction or promotion
$balance = $smsClient->checkBalance($route);

echo $balance->getCount(); // returns integer of available SMS credits count
```