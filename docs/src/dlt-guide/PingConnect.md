# Guideline for PingConnect

## Transactional

Any message which contains OTP and requires to complete a banking transaction initiated by bank customer will only be considered as transactional. Applies to all banks (national/scheduled/private/govt and even MNC.

### Example

* OTP messages required to complete the Debit Card/Credit Card/Pay through Mobile transactions.
* It's required to complete the net banking transactions.

![Sample Transactional SMS](/images/sample-transactional-sms.png)

| Actual Message                                                                                                                                  	| Template format while Registration                                                                                                                    	|
|-------------------------------------------------------------------------------------------------------------------------------------------------	|-------------------------------------------------------------------------------------------------------------------------------------------------------	|
| 824926 is the OTP for the transaction of INR 450.00 at Nyka with your icici card xx4281. OTP is valid for 15 mins. pls do not share with anyone 	| {#var#} is the OTP for the transaction of INR {#var#} at {#var#} with your icici card {#var#}. OTP is valid for {#var#}. pls do not share with anyone 	|
| 456739 is your OTP for fund transfer for the amount Rs.8,000. OTP valid for 10 minutes. Do not Share this OTP with anyone.                      	| {#var#} is your OTP for fund transfer for amount {#var#}. OTP valid for {#var#} minutes. Do not share this OTP with anyone.                           	|
| 123456 is OTP for your amazon Txn for the amount Rs 4,000 OTP valid for 5 minutes. Do not share this OTP with anyone.                           	| {#var#} is OTP for your eComm Txn for the amount {#var#} OTP valid for {#var#} minutes. Do not share this OTP with anyone                             	|

## Promotional

Any message intends to promote or sell a product, goods, or service is promotional in nature. These messages will be sent to customers based on the preference set by user and consent scrubbing function.

### Example

![Sample Promotional SMS](/images/sample-promotional-sms.png)


| Actual Message                                                                                                  	| Template format while Registration                                                                                  	|
|-----------------------------------------------------------------------------------------------------------------	|---------------------------------------------------------------------------------------------------------------------	|
| Free vouchers of LensKart, Pepperfry worth Rs.2000 with ICICI Bank Credit Card. SMS "apply" to 589098 TnC apply 	| Free vouchers of LensKart, Pepperfry worth Rs.{#var#} with ICICI Bank Credit Card. SMS "apply" to {#var#} TnC apply 	|
| Pay JUST Rs 4400* & get one-year salon services For Your Family. http://Ax5.in/laD4Tps                          	| Pay JUST Rs {#var#} & get {#var#} year salon services For Your Family. {#var#}                                      	|

## Service Explicit 

SMS sent to the registered customers after receiving their consent will come under the category of Service Explicit. As an example, If an ICICI Bank customer has given consent to receive promotional offers from ICICI, then these SMS will be included in the Service Explicit category. If, at any point of time, the customer withdraws and opt-out of receiving these texts, then the SMS will be considered Promotional.


## Service Implicit

Any message arising out of customer's actions or his existing relationship with the entity that is not promotional will be considered as Service-Implicit message. Important notifications regarding product and services such as purchase confirmation messages, order status, alerts, reminder SMS, etc. can be conveyed to the customers


## Instructions

### Copy Paste Option
1. Copy your desired message for content template and paste it in the message box
area.
2. Select the word(s) which you want to convert into a variable.
3. Click on the variable button.
4. Once the variable(s) entered from the top variable button, it should not be
modified manually in the message box.
5. Click the "Submit" button for submission of the Content Template request.


### Create New Message Option:

1. To type Message in a particular Language, Please select the preferred language
first and then type in the message.
2. Type the desired message for the content template in the message box.
3. To add variables, click on “Add Variable” button.
4. In case you wish add variable after typing the text messages, place the cursor
wherever the variable(s) need to to be added and click on “Add Variable” button.
5. Once the variable(s) entered, it should not be modified manually in the message
box.
6. Click the "Submit" button for submission of the Content Template request.


Note:- To create Template in Regional Language, please use "Copy/Paste Message option".

## Do's for Template

* Choose a relevant name for Template.
* Use promotional category for communications intended to send from numerical sender id only
* Variable {#var#} insertion to be required against values like date; amount; a/c no; OTP; names; etc…

## Dont's for Template

* Same content template against multiple headers.
* Selecting “Transactional” category by non-banking entities.
* Uisng extra spaces in Template.