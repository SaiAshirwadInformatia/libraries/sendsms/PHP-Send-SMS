# DLT Content Template Registration Process

SMS Content Template Registration is the next process for DLT and soon SMS will only be delivered if that SMS Content Template is approved on DLT. Kindly ensure to apply the SMS Content Templates separately for each kind of SMS that you are sending.

## Step 1

**Make a list of SMS Contents that you are sending**

You have to identify the SMS that you are sending. To help you identify and find your SMS contents, we suggest you to check these 2 suggestions:

**A)**. If you are using API, you can go to the Flow section in the left menu of your SMS panel and then download the recently used SMS Contents by clicking on the “Download Templates” button (there might be few more SMS which are not here, so check within your team for more such SMSs).

**B)**. You can also export the last 15-30 days' report from your SMS account and check for SMS contents there.


## Step 2

**Login to DLT Platform**

Kindly login to the DLT Platform where you have registered your Organisation or Entity.


## Step 3

**Go to Content Template Section**

Kindly go to the Content Template Section (and not the Consent Template Section) of the respective DLT Platform.


## Step 4

**Apply for New Content Template**

First check the FAQ Doc and then check the below Template Guidelines Docs and Videos specific to the DLT Platform on which you have registered. Then apply for Template Registration.

1. PingConnect (Videocon) - [Guidelines Doc](PingConnect.md) | Video 
2. VIL Power (Idea Vodafone)- Guidelines Doc | Tutorial Video_English  | Tutorial Video_Hindi
3. TrueConnect (Reliance Jio)- Guidelines Doc | Tutorial Video_English | Tutorial Video_Hindi
4. Airtel- Guidelines Doc | Video
5. Tata- Guidelines Doc
6. BSNL- Guidelines Doc
7. MTNL- Guidelines Doc


While applying Template on DLT Platform, kindly understand the below categories:
1. Service Implicit: All kinds of purely transactional SMS content should be registered here, like OTPs, Order Placement, Order Tracking, Passwords, etc.
2. Service Explicit: All kinds of promotional content should be registered here, like Discounts, Offers, New Launches, etc.
3. Transactional: All kinds of OTPs that banks will require to complete the banking transactions should be registered here. This category shall be used only by Banks.

Step 5: Changes in SMS API:
Once your SMS Templates are registered on DLT Platform, you have to follow this help doc to make the necessary changes on SMS API or Panel- http://9m.io/6Op6


Note: If you are an SMS Reseller, kindly share this Doc with your clients- http://9m.io/6Op2