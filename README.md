# Send SMS

Send SMS easily anywhere over the globe by registering on https://saiashirwad.in.

## Package Features

1. Validation of Country Code
2. Validation of mobile number by Country Code & known mobile prefix
3. Validation of mobile length by Country Code
4. Send Transaction SMS
5. Send Promotional SMS
6. Send OTP over SMS [Under Development]
7. Check balance easily

The verification feature is built on-top of authentic source [Wikipedia](https://en.wikipedia.org/wiki/List_of_mobile_telephone_prefixes_by_country)

## Benefits

1. Identify unknown numbers before, the library provides way's before you can even trigger SMS or wanted to register a number
2. Save on credits for sending on False number's, use them wisely with authentic numbers only
3. Easily integrate into any PHP application no matter what the framework is being used.

# Installation

```
composer require saiashirwadinformatia/sendsms
```

1. Register - https://saiashirwad.in
1. Retrieve API Key - https://saiashirwad.in/user/#api

# Usage

For security of credentials & identity, Send SMS package reads the `.env` or rather environment variables due to which the configs will stay secured outside git commits.

### Send SMS

```php
use SaiAshirwadInformatia\SendSMS;

/**
 * Retrieve your key from https://saiashirwad.in/user/#api
 *
 * Connect on support@saiashirwad.com for queries
 */
$apiKey = 'API_KEY';
// If API Key is given null, it will attempt to read from env as `SMS_SEND_KEY`

/**
 * Default is "91"
 */
$countryCode = '91';

/**
 * Seen on receivers phone a 6 characters Sender Id
 *
 * Default is "SAIMSG"
 *
 * Exact length required "6"
 */
$senderId = 'ABCDEF';

/**
 * Please note, this cannot be less than or greater than 6, should be exact 6 characters
 * Available Routes
 *  SendSMS::TRANSACTION_ROUTE => transaction (Received by DND)
 *  SendSMS::PROMOTIONAL_ROUTE => promotional (Ignored by DND)
 *
 * Please note, transaction require's explicit opt-in in your application
 * We may ask you this data, if any complains/reports received
 */
$route = SendSMS::TRANSACTION_ROUTE;

$smsClient = new SendSMS([
    'authkey' => $apiKey, 
    'countryCode' => $countryCode, 
    'senderId' => $senderId, 
    'route' => $route
]);

/**
 * Mobile number
 *
 * This can also be comma separate string for multiple phone numbers
 */
$mobile = '8888888888';

/**
 * Message upto 160 characters is considered as "1" credit
 * If it goes beyond the desired limit's credit's are charged accordingly
 */
$message = 'Hello, this is your message!';
$countryCode = '91'; // dynamically change country code during each call
$senderId = 'TESTMM'; // dynamically set sender id for each SMS
$dltTemplateId = ''; // This is now mandatory to send SMS in authentic way
$success = $smsClient->send($dltTemplateId, $mobile, $message, $countryCode, 

// returns SMS Message Id for tracking
echo $success->getMessage();
```

### Check Balance

```php
use SaiAshirwadInformatia\SendSMS;

/**
 * Retrieve your key from https://saiashirwad.in/user/#api
 *
 * Connect on support@saiashirwad.com for queries
 */
$apiKey = 'API_KEY';

// Default is Transaction route, alternate is SendSMS::PROMOTIONAL_ROUTE
$route = SendSMS::TRANSACTION_ROUTE;

$smsClient = new SendSMS();

// Alternate way
$smsClient->setAuthKey($apiKey);

// Fetch balance for selected route either transaction or promotion
$balance = $smsClient->checkBalance($route);

echo $balance->getCount(); // returns integer of available SMS credits count
```

## For Students / Trail Accounts

Free SMS credits (limited) will be given for college project students from India, drop a mail to support@saiashirwad.com

# License

MIT License

# Copyright

Sai Ashirwad Informatia, 2020
