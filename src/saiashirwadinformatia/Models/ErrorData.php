<?php

namespace SaiAshirwadInformatia\Models;

trait ErrorData
{
    /**
     * @var array
     */
    protected $mobiles = [];

    /**
     * @param $mobile
     * @param $message
     */
    public function reportError($mobile, \Exception $e)
    {
        $this->mobiles[] = [
            'mobile'  => $mobile,
            'message' => $e->getMessage(),
            'code'    => $e->getCode(),
        ];
    }

    public function hasErrors()
    {
        return count($this->mobiles) > 0;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->mobiles;
    }

    /**
     * @return mixed
     */
    public function reset()
    {
        $this->mobiles = [];

        return $this;
    }
}
