<?php

namespace SaiAshirwadInformatia\Models;

class URI
{
    const URL = "https://saiashirwad.in/";

    const SEND_SMS_URL = '/api/sendhttp.php';

    const BALANCE_CHECK_URL = '/api/balance.php';

    const OPTOUT_URL = '/api/optout.php';

    const FLOW_URL = '/api/v5/flow/';
}
