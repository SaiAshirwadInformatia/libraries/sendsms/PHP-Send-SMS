<?php

namespace SaiAshirwadInformatia\Models;

use SaiAshirwadInformatia\Exceptions\InvalidRouteException;

trait SMSData
{
    /**
     * @var mixed
     */
    private $authkey;

    /**
     * @var mixed
     */
    private $senderId;

    /**
     * @var mixed
     */
    private $route;

    /**
     * @var mixed
     */
    private $countryCode;

    /**
     * @var boolean
     */
    private $devMode = false;

    /**
     * @var mixed
     */
    private $balance = null;

    /**
     * @var boolean
     */
    private $encrypted = false;

    /**
     * @var mixed
     */
    private $exceptions = 'throw';

    /**
     * @return mixed
     */
    public function allowExceptions()
    {
        $this->exceptions = 'silent';

        return $this;
    }

    /**
     * @return mixed
     */
    public function throwExceptions()
    {
        $this->exceptions = 'throw';

        return $this;
    }

    /**
     * @param  $key
     * @return mixed
     */
    public function setAuthKey($key)
    {
        if ($key) {
            $this->authkey = $key;
        }

        return $this;
    }

    /**
     * @param  $senderId
     * @return mixed
     */
    public function setSenderId($senderId)
    {
        if ($senderId) {
            $this->senderId = $senderId;
        }

        return $this;
    }

    /**
     * @param  $route
     * @return mixed
     */
    public function setRoute($route)
    {
        if (in_array($route, [
            SMSRoute::TRANSACTION,
            SMSRoute::PROMOTIONAL,
        ])) {
            $this->route = $route;

            return $this;
        }

        InvalidRouteException::create($route);

    }

    /**
     * @param  $countryCode
     * @return mixed
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function enableDevMode()
    {
        $this->devMode = true;

        return $this;
    }

    /**
     * @return mixed
     */
    public function disableDevMode()
    {
        $this->devMode = false;

        return $this;
    }

    /**
     * @return mixed
     */
    public function enableEncryptedMessage()
    {
        $this->encrypted = true;

        return $this;
    }

    /**
     * @return mixed
     */
    public function disableEncryptedMessage()
    {
        $this->encrypted = false;

        return $this;
    }
}
