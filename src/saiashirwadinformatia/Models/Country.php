<?php
namespace SaiAshirwadInformatia\Models;

class Country
{
    /**
     * Data by country code
     *
     * @var array
     */
    private static $data = false;

    public static function init()
    {
        $reflection = new \ReflectionClass(self::class);
        if (!defined('DS')) {
            define('DS', DIRECTORY_SEPARATOR);
        }
        $dataFile = dirname(dirname($reflection->getFileName()));
        $dataFile .= DS . 'data' . DS . 'countries.json';
        self::$data = json_decode(\file_get_contents($dataFile), true);
    }

    public static function data()
    {
        if (!self::$data) {
            self::init();
        }
        return self::$data;
    }

    /**
     * @param $code
     */
    public static function hasCode($code)
    {
        if (!self::$data) {
            self::init();
        }
        return isset(self::$data[$code]);
    }

    /**
     * @param $code
     */
    public static function byCode($code)
    {
        if (!self::$data) {
            self::init();
        }
        if (!isset(self::$data[$code])) {
            return [];
        }
        return self::$data[$code];
    }
}
