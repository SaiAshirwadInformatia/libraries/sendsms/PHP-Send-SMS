<?php
namespace SaiAshirwadInformatia;

use Dotenv\Dotenv;
use Exception;
use GuzzleHttp\Client;
use SaiAshirwadInformatia\Exceptions\AuthKeyRequiredException;
use SaiAshirwadInformatia\Exceptions\FlowException;
use SaiAshirwadInformatia\Exceptions\InvalidRouteException;
use SaiAshirwadInformatia\Exceptions\SenderIdLengthException;
use SaiAshirwadInformatia\Exceptions\SenderIdRequiredException;
use SaiAshirwadInformatia\Models\Balance;
use SaiAshirwadInformatia\Models\ErrorData;
use SaiAshirwadInformatia\Models\SMSData;
use SaiAshirwadInformatia\Models\SMSRoute;
use SaiAshirwadInformatia\Models\Success;
use SaiAshirwadInformatia\Models\URI;
use SaiAshirwadInformatia\Services\MobileVerify;

class SendSMS
{
    use SMSData, ErrorData;

    /**
     * @var mixed
     */
    public $client;

    /**
     * @param $authkey
     * @param $countryCode
     * @param $senderId
     * @param $route
     */
    public function __construct($options = [])
    {
        $authkey = $options['authkey'] ?? null;

        if (!$authkey) {
            if (isset($_SERVER['SEND_SMS_KEY'])) {
                $authkey = $_SERVER['SEND_SMS_KEY'];
            } else {
                $envPath = __DIR__ . DIRECTORY_SEPARATOR;

                if (!file_exists($envPath . '.env')) {
                    $envPath .= '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR;
                }

                $dotenv = Dotenv::createMutable($envPath);
                $dotenv->safeLoad();

                $authkey = $_SERVER['SEND_SMS_KEY'] ?? null;
            }
        }
        if (!$authkey) {
            AuthKeyRequiredException::create();
        }
        $this->setAuthKey($authkey);

        $this->setSenderId($options['senderId'] ?? null);

        if (!$this->senderId && isset($_SERVER['SMS_SENDER_ID'])) {
            $this->setSenderId($_SERVER['SMS_SENDER_ID']);
        }
        $this->setRoute($options['route'] ?? SMSRoute::TRANSACTION);
        $this->setCountryCode($options['countryCode'] ?? '91');
    }

    private function prepareClient()
    {
        $this->client = new Client([
            'base_uri' => URI::URL,
            'debug' => $this->devMode
        ]);
    }

    /**
     * Check your pending SMS Credits
     *
     * @param  $route
     * @return $balance SaiAshirwadInformatia/Models/Balance
     */
    public function checkBalance($route = null)
    {
        $route = $route ?? $this->route;

        $postData = [
            'authkey'  => $this->authkey,
            'type'     => $route,
            'response' => 'json',
        ];

        $this->prepareClient();

        $output = $this->client->get(URI::BALANCE_CHECK_URL, [
            'query' => $postData,
        ]);
        $count         = intval($output->getBody()->getContents());
        $this->balance = new Balance($count);
        return $this->balance;
    }

    public function optout()
    {
        $this->prepareClient();

        $output = $this->client->get(URI::OPTOUT_URL, [
            'query' => [
                'authkey' => $this->authkey,
            ],
        ]);

        return json_decode($output->getBody()->getContents());
    }

    /**
     * @param $flowId
     * @param $recipents
     * @param $route
     */
    public function flow($flowId, $recipents, $route = 4, $scheduleAt = null)
    {
        foreach ($recipents as $index => $recipent) {
            if (isset($recipent['mobiles']) && strpos($recipent['mobiles'], $this->countryCode) !== 0) {
                $recipents[$index]['mobiles'] = $this->countryCode . $recipent['mobiles'];
            }
            try {
                MobileVerify::verify($this->countryCode, $recipent['mobiles']);
            } catch (\Exception $e) {
                $this->exception($recipent['mobiles'], $e);
                unset($recipents[$index]);
            }
        }

        $jsonBody = [
            'flow_id'    => $flowId,
            'sender'     => $this->senderId,
            'route'      => $route,
            'recipients' => $recipents,
            'unicode'    => 0,
        ];
        if ($scheduleAt) {
            $jsonBody['schtime'] = $scheduleAt;
        }
        if ($this->devMode) {
            $jsonBody['dev_mode'] = 1;
        }
        /*

        if ($this->encrypted) {
        $jsonBody['encrypt'] = 1;
        } else {
        $jsonBody['encrypt'] = 0;
        }

         */
        $this->prepareClient();

        $output = $this->client->post(URI::FLOW_URL, [
            'debug'   => $this->devMode,
            'json'    => $jsonBody,
            'headers' => [
                'authkey' => $this->authkey,
            ],
        ]);

        $response = json_decode($output->getBody()->getContents());
        if ('success' == $response->type) {
            return new Success($response->type, $response->message);
        }
        FlowException::create($response->message);
    }

    /**
     * @param $mobile
     * @param $message
     * @param $scheduleAt
     * @param $countryCode
     * @param null           $senderId
     * @param null           $route
     */
    protected function sendRaw($dltTemlpateId, $mobile, $message, $delay = 0, $scheduleAt = null, $countryCode = null, $senderId = null, $route = null, $unicode = null)
    {
        if (!$this->senderId) {
            SenderIdRequiredException::create();
        }

        $countryCode = $countryCode ?? $this->countryCode;

        $senderId = $senderId ?? $this->senderId;
        $route    = $route ?? $this->route;

        if (\strlen($senderId) != 6) {
            SenderIdLengthException::create($senderId);
        }
        if (!in_array($route, [SMSRoute::TRANSACTION, SMSRoute::PROMOTIONAL])) {
            InvalidRouteException::create($route);
        }

        if (!is_array($mobile)) {
            $mobile = explode(",", $mobile);
        }
        foreach ($mobile as $m) {
            try {
                MobileVerify::verify($countryCode, $m);
            } catch (Exception $e) {
                $this->exception($m, $e);
            }
        }
        $postData = [
            'authkey'   => $this->authkey,
            'mobiles'   => implode(",", $mobile),
            'message'   => urlencode($message),
            'schtime'   => $scheduleAt,
            'country'   => $countryCode,
            'sender'    => $senderId,
            'route'     => $route,
            'DLT_TE_ID' => $dltTemlpateId,
        ];
        if ($scheduleAt) {
            $postData['schtime'] = $scheduleAt;
        }
        if ($delay) {
            $postData['afterminutes'] = $delay;
        }
        if ($this->devMode) {
            $postData['dev_mode'] = 1;
        }
        if ($this->encrypted) {
            $postData['encrypt'] = 1;
        }
        
        $this->prepareClient();
        
        $output = $this->client->post(URI::SEND_SMS_URL, ['form_params' => $postData]);

        return $output->getBody()->getContents();
    }

    /**
     * @param  $mobile
     * @param  $message
     * @param  $scheduleAt    -           yyyy-mm-dd hh:MM:ss
     * @param  $countryCode
     * @param  null           $senderId
     * @param  null           $route
     * @return mixed
     */
    public function schedule($dltTemlpateId, $mobile, $message, $scheduleAt, $countryCode = null, $senderId = null, $route = null)
    {
        return $this->sendRaw($dltTemlpateId, $mobile, $message, 0, $scheduleAt, $countryCode, $senderId, $route);
    }

    /**
     * Send SMS to one or many mobile numbers
     *
     * @param  $mobile        -           string|array
     * @param  $message
     * @param  $countryCode
     * @param  null           $senderId
     * @param  null           $route
     * @return mixed
     */
    public function send($dltTemlpateId, $mobile, $message, $countryCode = null, $senderId = null, $route = null)
    {
        $messageId = $this->sendRaw($dltTemlpateId, $mobile, $message, 0, null, $countryCode, $senderId, $route);
        return new Success('success', $messageId);
    }

    /**
     * @param  $mobile
     * @param  $message
     * @param  $countryCode
     * @param  null           $senderId
     * @return mixed
     */
    public function sendPromotional($dltTemlpateId, $mobile, $message, $countryCode = null, $senderId = null)
    {
        return $this->sendRaw($dltTemlpateId, $mobile, $message, 0, null, $senderId, $countryCode, SMSRoute::PROMOTIONAL);
    }

    /**
     * @param  $mobile
     * @param  $message
     * @param  $countryCode
     * @param  null           $senderId
     * @return mixed
     */
    public function sendTransactional($dltTemlpateId, $mobile, $message, $countryCode = null, $senderId = null)
    {
        return $this->sendRaw($dltTemlpateId, $mobile, $message, 0, null, $senderId, $countryCode, SMSRoute::TRANSACTION);
    }

    /**
     * @param $mobile
     * @param \Exception $e
     */
    public function exception($mobile, \Exception $e)
    {
        if ('throw' == $this->exceptions) {
            throw $e;
        }
        $this->reportError($mobile, $e);
    }
}
