<?php
namespace SaiAshirwadInformatia\Services;

use SaiAshirwadInformatia\Exceptions\InvalidCountryCodeException;
use SaiAshirwadInformatia\Exceptions\InvalidMobileLengthException;
use SaiAshirwadInformatia\Exceptions\InvalidMobilePrefixException;
use SaiAshirwadInformatia\Models\Country;

class MobileVerify
{
    /**
     * @var mixed
     */
    public static $STRICT_VERIFY_PREFIX = true;

    /**
     * @param $code
     */
    public static function isValidCode($code)
    {
        return Country::hasCode($code);
    }

    /**
     * @param  $code
     * @param  $mobile
     * @return mixed
     */
    public static function isValidLength($code, $mobile)
    {
        $isValid   = false;
        $countries = Country::byCode($code);
        $noLength  = 0;
        foreach ($countries as $country) {
            if (!isset($country['length']) or count($country['length']) == 0) {
                $noLength++;
                continue;
            }
            if (in_array(strlen($mobile), $country['length'])) {
                $isValid = true;
            }
        }
        if (count($countries) == $noLength) {
            $isValid = true;
        }
        return $isValid;
    }

    /**
     * @param $code
     * @param $mobile
     */
    public static function isValidPrefix($code, $mobile)
    {
        $isValid    = false;
        $prefixes   = [];
        $countries  = Country::byCode($code);
        $noPrefixes = 0;
        foreach ($countries as $country) {
            if (!isset($country['prefix']) or count($country['prefix']) == 0) {
                $noPrefixes++;
                continue;
            }
            foreach ($country['prefix'] as $prefix) {
                if (!in_array($prefix, $prefixes)) {
                    $prefixes[] = $prefix;
                }
                if (strpos($mobile, $prefix) === 0) {
                    $isValid = true;
                }
            }
        }
        if (count($countries) == $noPrefixes) {
            return true;
        }
        return $isValid;
    }

    /**
     * @param $code
     * @param $mobile
     */
    public static function isValid($code, $mobile)
    {
        $isValid = self::isValidCode($code) && self::isValidLength($code, $mobile);
        if (self::$STRICT_VERIFY_PREFIX) {
            $isValid = self::isValidPrefix($code, $mobile);
        }
        return $isValid;
    }

    /**
     * @param $code
     */
    public static function verifyCode($code)
    {
        if (!self::isValidCode($code)) {
            InvalidCountryCodeException::create($code);
        }
    }

    /**
     * @param $code
     * @param $mobile
     */
    public static function verifyLength($code, $mobile)
    {
        if (!self::isValidLength($code, $mobile)) {
            $allowed   = [];
            $countries = Country::byCode($code);
            foreach ($countries as $country) {
                if (!in_array($country['length'], $allowed)) {
                    $allowed[] = $country['length'];
                }
            }
            InvalidMobileLengthException::create($mobile, \strlen($mobile), $allowed);
        }
    }

    /**
     * @param $code
     * @param $mobile
     */
    public static function verifyPrefix($code, $mobile)
    {
        $prefixes  = [];
        $countries = Country::byCode($code);
        foreach ($countries as $country) {
            if (!isset($country['prefix'])) {
                continue;
            }
            foreach ($country['prefix'] as $prefix) {
                if (!in_array($prefix, $prefixes)) {
                    $prefixes[] = $prefix;
                }
            }
        }
        if (!self::isValidPrefix($code, $mobile)) {
            InvalidMobilePrefixException::create($mobile, $prefixes);
        }
    }

    /**
     * @param $code
     * @param $mobile
     */
    public static function verify($code, $mobile)
    {
        self::verifyCode($code);

        self::verifyLength($code, $mobile);

        if (self::$STRICT_VERIFY_PREFIX) {
            self::verifyPrefix($code, $mobile);
        }
    }
}
