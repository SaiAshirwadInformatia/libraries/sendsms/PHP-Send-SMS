<?php

namespace SaiAshirwadInformatia\Services;

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;

class GenerateDataByCountry
{
    public function process()
    {
        $host = 'http://localhost:4444';

        $driver = RemoteWebDriver::create($host, DesiredCapabilities::chrome());
        $driver->get("https://en.wikipedia.org/wiki/List_of_mobile_telephone_prefixes_by_country");
        $driver->manage()->window()->maximize();
        $driver->wait(15, 5000)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('mw-headline'))
        );
        $rows = $driver->findElements(WebDriverBy::cssSelector('.jquery-tablesorter tbody tr'));

        $skipCode        = 0;
        $skipSize        = 0;
        $skipCarrier     = 0;
        $skipNotes       = 0;
        $countries       = [];
        $previousCountry = [];
        foreach ($rows as $row) {
            $cells = $row->findElements(WebDriverBy::cssSelector('td'));
            $start = 0;
            $end   = 0;
            if (count($cells) == 6) {
                $skipCountries = 0;
                $skipCode      = 0;
                $skipSize      = 0;
                $skipCarrier   = 0;
                $skipNotes     = 0;
                $rowspan       = $cells[1]->getAttribute('rowspan');
                if ($rowspan) {
                    $skipCode = intval($rowspan);
                }
                $length          = $cells[3]->getText();
                $previousCountry = [
                    'name'   => $cells[0]->getText(),
                    'code'   => substr($cells[1]->getText(), 1),
                    'prefix' => $cells[2]->getText(),
                ];
                if (strpos($length, ',') !== false or strpos($length, '-') !== false or strpos($length, 'to') !== false) {
                    if (strpos($length, '-') !== false) {
                        $length = explode('-', $length);
                    } else if (strpos($length, ',') !== false) {
                        $length = explode(',', $length);
                    } else {
                        $length = explode('to', $length);
                    }
                    $length[0] = intval(str_replace(' ', '', $length[0]));
                    $length[1] = intval(str_replace(' ', '', $length[1]));
                    for ($i = $length[0]; $i <= $length[1]; $i++) {
                        $previousCountry['length'] = $i;
                        $countries[]               = $previousCountry;
                    }
                } else {
                    $previousCountry['length'] = $cells[3]->getText();

                    $countries[] = $previousCountry;
                }
            } else if (count($cells) == 5) {
                $previousCountry['code']   = substr($cells[1]->getText(), 1);
                $previousCountry['prefix'] = $cells[2]->getText();
                $previousCountry['length'] = $cells[3]->getText();
            } else {
                if (count($cells) == 4) {
                    $previousCountry['length'] = str_replace('?', '', $cells[1]->getText());
                    $prefix                    = $cells[0]->getText();
                } else {
                    $prefix = $cells[0]->getText();
                }
                $prefix = str_replace('x', '', $prefix);
                $prefix = str_replace(' ', '', $prefix);

                if (strpos($prefix, ',') !== false) {
                    $prefix                    = explode(',', $prefix);
                    $prefix[0]                 = trim($prefix[0]);
                    $prefix[1]                 = trim($prefix[1]);
                    $previousCountry['prefix'] = $prefix[0];
                    $countries[]               = $previousCountry;
                    $previousCountry['prefix'] = $prefix[1];
                    $countries[]               = $previousCountry;
                } else if (strpos($prefix, '-') !== false or strpos($prefix, 'or') !== false) {
                    if (strpos($prefix, '-') !== false) {
                        $prefix = explode('-', $prefix);
                    } else if (strpos($prefix, 'or') !== false) {
                        $prefix = explode('or', $prefix);
                    }

                    $temp_prefix = '';
                    $prefix[0]   = trim($prefix[0]);
                    $prefix[1]   = trim($prefix[1]);
                    if (strlen($prefix[0]) == 4) {
                        $temp_prefix = substr($prefix[0], 0, 2);
                        $prefix[0]   = substr($prefix[0], 2);
                        $prefix[1]   = substr($prefix[1], 2);
                    }
                    $start   = intval($prefix[0]);
                    $end     = intval($prefix[1]);
                    $counter = 0;
                    for ($i = $start; $i <= $end; $i++) {
                        $counter++;
                        $previousCountry['prefix'] = $temp_prefix . $i;
                        $countries[]               = $previousCountry;
                    }
                } else {
                    if (!is_numeric($prefix)) {
                        continue;
                    }
                    $previousCountry['prefix'] = $prefix;
                    $countries[]               = $previousCountry;
                }
            }
        }
        $finalCountries = [];
        foreach ($countries as $country) {
            if (!isset($finalCountries[$country['code']])) {
                $finalCountries[$country['code']] = [
                    $country['name'] => [
                        'name'   => $country['name'],
                        'code'   => $country['code'],
                        'prefix' => [$country['prefix']],
                        'length' => [$country['length']],
                    ],
                ];
                continue;
            }
            if (!isset($finalCountries[$country['code']][$country['name']])) {
                $finalCountries[$country['code']][$country['name']] = [
                    'name'   => $country['name'],
                    'code'   => $country['code'],
                    'prefix' => [$country['prefix']],
                    'length' => [$country['length']],
                ];
                continue;
            }
            if (!in_array($country['prefix'], $finalCountries[$country['code']][$country['name']]['prefix'])) {
                $finalCountries[$country['code']][$country['name']]['prefix'][] = $country['prefix'];
            }
            if (!in_array($country['length'], $finalCountries[$country['code']][$country['name']]['length'])) {
                $finalCountries[$country['code']][$country['name']]['length'][] = $country['length'];
            }
        }
        $reflection = new \ReflectionClass(self::class);
        if (!defined('DS')) {
            define('DS', DIRECTORY_SEPARATOR);
        }
        $dataFile = dirname(dirname($reflection->getFileName())) . DS . 'data' . DS . 'countries.json';
        file_put_contents($dataFile, json_encode($finalCountries));
    }
}
