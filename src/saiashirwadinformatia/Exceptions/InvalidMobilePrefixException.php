<?php

namespace SaiAshirwadInformatia\Exceptions;

class InvalidMobilePrefixException extends \Exception
{
    /**
     * @param $expected
     * @param $received
     */
    public static function create($received, $expected)
    {
        throw new self("Invalid prefix of mobile number received {$received}, expected: " . json_encode($expected));
    }

}
