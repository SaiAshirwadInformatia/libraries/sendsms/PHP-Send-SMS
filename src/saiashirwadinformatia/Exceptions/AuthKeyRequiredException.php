<?php

namespace SaiAshirwadInformatia\Exceptions;

class AuthKeyRequiredException extends \Exception
{

    public static function create()
    {
        throw new self("Auth API Key is required in-order to send SMS, please generate by visiting on https://saiashirwad.in");
    }
}
