<?php

namespace SaiAshirwadInformatia\Exceptions;

class InvalidRouteException extends \Exception
{
    /**
     * @param $expected
     */
    public static function create($received)
    {
        throw new self("Invalid route received as {$received}, expected: [1 => PROMOTIONAL, 4 => TRANSACTIONAL] smses");
    }
}
