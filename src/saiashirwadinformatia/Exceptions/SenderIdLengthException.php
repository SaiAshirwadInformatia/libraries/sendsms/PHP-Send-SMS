<?php

namespace SaiAshirwadInformatia\Exceptions;

class SenderIdLengthException extends \Exception
{
    /**
     * @param $length
     */
    public static function create($senderId)
    {
        $length = strlen($senderId);
        throw new self("Invalid length of Sender Id received as {$length}, expected length is exactly 6");
    }

}
