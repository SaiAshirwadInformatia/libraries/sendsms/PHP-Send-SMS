<?php

namespace SaiAshirwadInformatia\Exceptions;

class InvalidCountryCodeException extends \Exception
{
    /**
     * @param $code
     */
    public static function create($code)
    {
        throw new self("Invalid country code {$code} was provided, kindly verify and try again");
    }
}
