<?php

namespace SaiAshirwadInformatia\Exceptions;

class InvalidMobileLengthException extends \Exception
{
    /**
     * @param $expected
     * @param $received
     */
    public static function create($mobile, $received, $expected)
    {
        throw new self("Invalid length of mobile {$mobile} number received {$received}, expected: " . json_encode($expected));
    }
}
