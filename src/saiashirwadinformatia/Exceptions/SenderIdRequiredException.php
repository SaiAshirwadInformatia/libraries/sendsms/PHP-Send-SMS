<?php

namespace SaiAshirwadInformatia\Exceptions;

class SenderIdRequiredException extends \Exception
{

    public static function create()
    {
        throw new self("Sender ID is required for sending SMS, please register your SMS Header with DLT");
    }
}
