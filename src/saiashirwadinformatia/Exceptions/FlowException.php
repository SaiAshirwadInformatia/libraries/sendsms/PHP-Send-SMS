<?php

namespace SaiAshirwadInformatia\Exceptions;

class FlowException extends \Exception
{

    /**
     * @param $message
     */
    public static function create($message)
    {
        throw new self("FLOW API Failed with error: " . $message);
    }
}
