<?php

use SaiAshirwadInformatia\Models\SMSRoute;
use SaiAshirwadInformatia\SendSMS;

$smsClient = new SendSMS();

test('check if optout API is working', function () use ($smsClient) {
    $output = $smsClient->optout(SMSRoute::TRANSACTION);

    $this->assertNotEmpty($output);
});
