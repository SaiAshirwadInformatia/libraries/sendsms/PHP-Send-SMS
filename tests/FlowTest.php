<?php

use SaiAshirwadInformatia\Exceptions\InvalidMobileLengthException;
use SaiAshirwadInformatia\SendSMS;

$smsClient = new SendSMS();
if(!isset($_SERVER['FLOW_SENDER'])) {
    return;
}
$smsClient->setSenderId($_SERVER['FLOW_SENDER']);
test("check flow api is working", function () use ($smsClient) {
    $output = $smsClient->flow($_SERVER['FLOW_ID'], [
        [
            'mobiles' => $_SERVER['SMS_TEST_NUMBER'],
            "name"    => "Dr",
            "book"    => "Hello World",
            "email"   => "rohan@saiashirwad.com",
        ],
    ]);

    $this->assertNotEmpty($output);

    $this->assertEmpty($smsClient->getErrors());
});

test("check flow api breaks with invalid mobile length", function () use ($smsClient) {

    $this->expectException(InvalidMobileLengthException::class);

    $output = $smsClient->flow($_SERVER['FLOW_ID'], [
        [
            'mobiles' => 4444,
            "name"    => "Dr",
            "book"    => "Hello World",
            "email"   => "rohan@saiashirwad.com",
        ],
    ]);

    $this->assertNotEmpty($output);
});

test("check flow api breaks with invalid mobile length when short mobile given as string", function () use ($smsClient) {

    $this->expectException(InvalidMobileLengthException::class);

    $output = $smsClient->flow($_SERVER['FLOW_ID'], [
        [
            'mobiles' => "55555",
            "name"    => "Dr",
            "book"    => "Hello World",
            "email"   => "rohan@saiashirwad.com",
        ],
    ]);

    $this->assertNotEmpty($output);
});

test("check flow api breaks with silent exception for invalid mobile length", function () use ($smsClient) {

    $output = $smsClient->allowExceptions()->flow($_SERVER['FLOW_ID'], [
        [
            'mobiles' => "55555",
            "name"    => "Dr",
            "book"    => "Hello World",
            "email"   => "rohan@saiashirwad.com",
        ],
    ]);

    $this->assertNotEmpty($output);

    $this->assertNotEmpty($smsClient->getErrors());
});