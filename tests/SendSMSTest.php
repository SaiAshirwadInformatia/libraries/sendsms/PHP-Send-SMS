<?php

use SaiAshirwadInformatia\Models\SMSRoute;
use SaiAshirwadInformatia\SendSMS;

$smsClient = new SendSMS();

test('check if normal message sending is working with DTL Template 1', function () use ($smsClient) {
    $dltTemplateId1 = $_SERVER['DLT_1'];
    $sender         = $_SERVER['SMS_SENDER_ID'] ?? 'SAIINF';
    $testNumber     = $_SERVER['SMS_TEST_NUMBER'];
    $message        = $_SERVER['MSG1'];
    $success        = $smsClient->send(
        $dltTemplateId1,
        $testNumber,
        $message,
        '91',
        $sender);
    $this->assertEquals('success', $success->getType());
});

test('check if encrypted message are working', function () use ($smsClient) {
    $dltTemplateId1 = $_SERVER['DLT_1'];
    $sender         = $_SERVER['SMS_SENDER_ID'] ?? 'SAIINF';
    $testNumber     = $_SERVER['SMS_TEST_NUMBER'];
    $message        = $_SERVER['MSG2'];
    $success        = $smsClient->enableEncryptedMessage()->send(
        $dltTemplateId1,
        $testNumber,
        $message,
        '91',
        $sender);
    $this->assertEquals('success', $success->getType());
});

test('check if DLT Dev Mode works & sends SMS', function () use ($smsClient) {
    $dltTemplateId1 = $_SERVER['DLT_1'];
    $sender         = $_SERVER['SMS_SENDER_ID'] ?? 'SAIINF';
    $testNumber     = $_SERVER['SMS_TEST_NUMBER'];
    $message        = $_SERVER['MSG2'];
    $success        = $smsClient->enableDevMode()->send(
        $dltTemplateId1,
        $testNumber,
        $message,
        '91',
        $sender);
    $this->assertEquals('success', $success->getType());
});

test('check balance API is working', function () use ($smsClient) {
    $balance = $smsClient->checkBalance(SMSRoute::TRANSACTION);
    $this->assertGreaterThanOrEqual(0, $balance->getCount());
});
