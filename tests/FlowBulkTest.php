<?php

use SaiAshirwadInformatia\Exceptions\InvalidMobileLengthException;
use SaiAshirwadInformatia\SendSMS;

$smsClient = new SendSMS();
$smsClient->setSenderId('SAIASH');

test("check flow api is working", function () use ($smsClient) {

    $mobiles = json_decode($_SERVER['TEST_NUMBERS'], true);

    foreach(array_chunk($mobiles, 5) as $mArr) {
        $output = $smsClient->flow($_SERVER['FLOW_ID'], $mArr);
        var_dump($output);
        $this->assertNotEmpty($output);
    }
    
    $this->assertEmpty($smsClient->getErrors());
});