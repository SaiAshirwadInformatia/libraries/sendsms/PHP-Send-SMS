<?php

use SaiAshirwadInformatia\Exceptions\InvalidMobileLengthException;
use SaiAshirwadInformatia\Exceptions\InvalidMobilePrefixException;
use SaiAshirwadInformatia\Services\MobileVerify;

test('check proper numbers are validating correctly', function () {
    $this->assertTrue(MobileVerify::isValidCode('91'));
    $this->assertTrue(MobileVerify::isValidPrefix('91', '9892098920'));
    $this->assertTrue(MobileVerify::isValidPrefix('91', '8979638771'));
});

test('check wrong numbers are validating correctly', function () {
    $this->assertFalse(MobileVerify::isValidCode('999'));
    $this->assertFalse(MobileVerify::isValidPrefix('91', '662098920'));
});

test('check invalid mobile length exception is noticed by country code', function () {
    $this->expectException(InvalidMobileLengthException::class);
    MobileVerify::verify("91", "9892098");
});

test('check mobile number prefix is invalid based by country code', function () {
    $this->expectException(InvalidMobilePrefixException::class);
    MobileVerify::verify("91", "6598765267");
});
